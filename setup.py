from distutils.core import setup
from Cython.Build import cythonize


setup(
    name="Test App",
    ext_modules=cythonize("*.pyx")
)
