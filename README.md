# Cython Test #

A simple program written to experiment with Cython and PyPy.

The program implements a simple numerical integration algorithm in pure Python, and in Cython with static type declarations and calls to the native C math library. The algorithms are timed over many repeat calculations.

### Getting Started ###
First, install Cython via Pip

`$ pip install -r requirements.txt`

Build the cython_functions.pyx Cython module

`$ python setup.py build_ext --inplace`

Run the program

`$ python main.py`

Example output:
```
$ python main.py 
python_functions.integrate_f
0.886326925453
Duration: 9.25257897377 seconds
cython_functions.integrate_f
0.886326925453
Duration: 0.214478969574 seconds
OK
```

Run the program using PyPy, without the Cython part

`$ pypy main_pypy.py`

Example output:
```
$ pypy main_pypy.py 
python_functions.integrate_f
0.886326925453
Duration: 0.571019887924 seconds
OK
```

### Discussion ###
* The static typed and compiled Cython version of the algorithm is 44 times faster than the pure Python version
* The PyPy interpreter runs the pure Python algorithm 16 times faster than the CPython interpreter
* The computed result is exactly equal in all three cases; it differs slightly from the Wolfram Alpha result since the numerical algorithm used is linear
* This is a toy problem; Python libraries such as NumPy should be used in real world scenarios and are equivalently fast
* Cython manages memory automatically and dramatically simplifies the interface between Python and C code
* Cython code is almost as easy to understand as pure Python, and typing and cdefs can be added incrementally to Python code.


### More Information ###
* [Calculation on Wolfram Alpha](https://www.wolframalpha.com/input/?i=integrate+exp(-x%5E2)+from+x%3D0+to+x%3D10)
* [Cython website](http://cython.org/)
* [Calling C functions in Cython](http://docs.cython.org/src/tutorial/external.html)
* [GNU libc math exp function](http://www.gnu.org/software/libc/manual/html_node/Exponents-and-Logarithms.html#Exponents-and-Logarithms)
* [PyPy website](http://pypy.org/)
* [Just-in-time compilation Wikipedia article](https://www.wikiwand.com/en/Just-in-time_compilation)