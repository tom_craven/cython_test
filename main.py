import python_functions
import cython_functions


if __name__ == "__main__":
    REPEAT_COUNT = 500
    N = 50000

    print "python_functions.integrate_f"
    python_functions.repeatedly_call_function(
        REPEAT_COUNT,
        python_functions.integrate_f, 0.0, 10.0, N)

    print "cython_functions.integrate_f"
    python_functions.repeatedly_call_function(
        REPEAT_COUNT,
        cython_functions.integrate_f, 0.0, 10.0, N)

    print "OK"
