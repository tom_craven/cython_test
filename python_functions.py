import math
import time


def f(x):
    return math.exp(-(x ** 2))


def integrate_f(a, b, N):
    s = 0
    dx = (b - a) / N
    for i in range(N):
        s += f(a + i * dx)
    return s * dx


def repeatedly_call_function(count, fn, *args, **kwargs):
    t0 = time.time()
    for i in range(count):
        result = fn(*args, **kwargs)

    print result
    print "Duration: {} seconds".format(time.time() - t0)
